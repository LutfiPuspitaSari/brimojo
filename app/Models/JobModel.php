<?php 
namespace App\Models;
use CodeIgniter\Model;

class JobModel extends Model
{
    protected $table = 'job_list';

    public function __construct(){
        parent::__construct();
        $db = \Config\Database::connect();
        $builder = $db->table('job_list');
    }

    public function getJob($id = false)
    {
        if($id === false) {
            return $this->findAll();
        } else {
            return $this->getWhere(['job_id' => $id]);
        }
    }

    public function saveJob($data)
    {
        $builder = $this->db->table($this->table);
        return $builder->insert($data);
    }

    public function editJob($data, $id)
    {
        $builder = $this->db->table($this->table);
        $builder->where('job_id', $id);
        return $builder->update($data);
    }

    public function deleteJob($id)
    {
        $builder = $this->db->table($this->table);
        return $builder->delete(['job_id' => $id]);
    }

    public function getTotalJobs()
    {
        return $this->db->table('job_list')->countAll();
    }
}