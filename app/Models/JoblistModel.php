<?php 
namespace App\Models;
use CodeIgniter\Model;

class JoblistModel extends Model
{
    protected $table = 'vw_job_list';

    public function __construct(){
        parent::__construct();
        $db = \Config\Database::connect();
        $builder = $db->table('vw_job_list');
    }

    public function getJob($id = false)
    {
        if($id === false) {
            return $this->findAll();
        } else {
            return $this->getWhere(['job_id' => $id]);
        }
    }

}