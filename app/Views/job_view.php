<!doctype html>
<html lang="en">
  <head>
    <title><?= $title;?></title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <!-- <?php echo "<link rel='stylesheet' type='text/css' href='job_view.css' />"; ?> -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <style>
        #nav-header{
            background-color: #09539C;
        }
        .navbar-brand{
            color: white;
        }
        #formSearch{
            width: 419px;
            margin-left: 15px;
            margin-right: 535px;
        }
        .buttonAdd{
            border: 1px solid #09539C;
            box-sizing: border-box;
            border-radius: 30px;
            background: #EEF8FD;
            width: 160px;
        }
        .buttonAdd img{
            width: 20px;
            height: 20px;
            margin-left: 10px;
        }
        .tableTotal{
            margin-right: 0px;
            margin-left: auto;
            margin-bottom: 10px;
            color: #09539C;
        }
        .trTotalJobs{
            text-align: center;
            padding-left: 20px;
            padding-right: 20px;
        }
    </style>

  </head>
<body>
    <nav class="navbar navbar-expand-sm navbar-dark" id="nav-header">
        <div class="container">
            <a class="navbar-brand" href="<?= base_url();?>"><h3>BRI MOJO</h3></a>
            <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
                aria-expanded="false" aria-label="Toggle navigation"></button>
        </div>
    </nav>
    <div class="container pt-5">
        <h5 ></h5>
        <div class="row">
            <div id="formSearch">
                <input class="form-control" id="myInput" type="text" placeholder="Search..">
            </div>
            <div class="buttonAdd">
                <img src="<?= $iconAdd; ?>"/>
                <a href="<?= base_url('job/add');?>" class="btn btn-default">Add new job</a> <!-- manggil function addNew dr controller job-->
            </div>
        </div>
        
        <br>
        <table class="tableTotal">
            <td class="trTotalJobs">
                <?= $totalServers?><br>
                Total Jobs
            </td>
            <td class="trTotalJobs">
                <?= $totalJobs?><br>
                Total Jobs
            </td>
        </table>
        
        <div class="card">
            <div class="card-header bg-info text-white">
                <h4 class="card-title"><?= $title; ?></h4>
                
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No. </th>
                                <th>Ip address</th>
                                <th>Job Name</th>
                                <th>Migration Staging</th>
                                <th>Migration EOM</th>
                                <th>Status Running</th>
                                <th>Aksi</th>
                            </tr> 
                        </thead>
                        <tbody id="myTable">
                            <?php $no=1; foreach($job as $isi){?> <!-- define job dr model $data['job'] ke variable isi-->
                                <tr>
                                    <td><?= $no;?></td>
                                    <td><?= $isi['server_ip'];?></td>
                                    <td><?= $isi['job_name'];?></td>
                                    <td><?= $isi['migrasi_staging'];?></td>
                                    <td><?= $isi['migrasi_eom'];?></td>
                                    <td><?= $isi['status_running'];?></td>
                                    <td>
                                        <a href="<?= base_url('job/edit/'.$isi['job_id']);?>" 
                                        class="btn btn-success">
                                        Edit</a>
                                        <a href="<?= base_url('job/delete/'.$isi['job_id']);?>" 
                                        onclick="javascript:return confirm('Are you sure to delete this job?')"
                                        class="btn btn-danger">
                                        Delete</a>

                                    </td>
                                </tr>
                            <?php $no++;}?>
                        </tbody>  

                    </table>
                </div>
            </div>
        </div>
    </div>

    <script>
    $(document).ready(function(){
    $("#myInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#myTable tr").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
    });
    </script>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
