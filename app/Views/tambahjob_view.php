<div class="container p-3">
    <a href="<?= base_url('job');?>" class="btn btn-secondary mb-2">Kembali</a>
    <div class="card">
        <div class="card-header bg-info text-white">
            <h4 class="card-title"><?= $title;?></h4>
        </div>
        <div class="card-body">
            <form method="post" action="<?= base_url('job/createNewJob');?>">
                <div class="form-group">
                    <label for="">Job name</label>
                    <input type="text" name="jobname" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="">IP Address</label>
                    <!-- <input type="text" name="serverid" class="form-control" required> -->
                    <select class="form-control" id="sel1" name="serverid">
                        <option> </option>
                    <?php $no=1; foreach($servers as $row){?>
                        <option><?= $row['server_ip'] ?></option>
                    <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                <label for="sel1">Status migrasi</label>
                    <select class="form-control" id="sel1" name="migrasi">
                        <option> </option>
                        <option>in progress</option>
                        <option>done</option>
                    </select>
                </div>
                <!-- <div class="form-group">
                    <label for="">Running status</label>
                    <input type="text" name="running" class="form-control" >
                </div> -->
                <button class="btn btn-success" >save</button>
            </form>
            
        </div>
    </div>
</div>